$(document).ready(function() {
    $('.burger').on('click', function() {
        $(this).toggleClass('active');
        $('nav').slideToggle('400');
    });



    $(".slick-arrow").html('<i class="fa fa-angle-left"></i>');
    $(".slick-arrow").html('<i class="fa fa-angle-right"></i>');


    // Равная высота колонок
    $(function($) {
        var max_col_height = 0; // максимальная высота, первоначально 0
        $('.service-content .green-content').each(function() { // цикл "для каждой из колонок"
            if ($(this).height() > max_col_height) { // если высота колонки больше значения максимальной высоты,
                max_col_height = $(this).height(); // то она сама становится новой максимальной высотой
            }
        });
        $('.service-content .green-content').height(max_col_height); // устанавливаем высоту каждой колонки равной значению максимальной высоты
    });

    $('.faq-content article').not(':first').hide();
    $('.faq-content .bg-title').click(function(){

        var findArticle = $(this).next('article');
        var findWrapper = $(this).closest('.faq-content');

        if (findArticle.is(':visible')) {
            findArticle.slideUp();
        }
        else {
            findWrapper.find('article').slideUp();
            findArticle.slideDown();
        }
    });

    $('.hidden-text').hide();
    $('.btn-footer').on('click', function() {
        $('.btn-footer').toggleClass('active');
        $('.footer-bot').toggleClass('footer-line');
       if ($('.hidden-text').is(':visible')) {
           $('.hidden-text').slideUp();
           $('.footer-top').removeAttr('style');
       }
       else {
            $('.hidden-text').slideDown();
            $('.footer-top').css({transition: 'background-color .3s ease-in-out', backgroundColor: '#435c91'});
       }
        return false;
     });

     var screen = $(window).width();
     if (screen <= 991) {
         $('.slider').slick({
             autoplay: true,
             autoplaySpeed: 2000,
             arrows: true,
             nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
             prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>'
         });
     };
});
